﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _050820
{

    interface ISuperHero
    {
        void ActivateSuperPowers();
    }
    interface IFlash : ISuperHero
    {
        void FireLightning();
    }
    interface IClimb : ISuperHero
    {
        void Climb();
    }
    interface IFly : ISuperHero
    {
        void Fly();
    }
    public abstract class Human
    {
        protected int _age;
        public abstract string GetName();
        public Human(int age)
        {
            this._age = age;
        }
    }
    public class Flash : Human, IFlash
    {
        public Flash(int age) : base(age)
        {
        }

        public void ActivateSuperPowers()
        {
            FireLightning();
        }

        public void FireLightning()
        {
            Console.WriteLine("Flash gordon is firing lightning!");
        }

        public override string GetName()
        {
            return "Flash gordon!";
        }
    }

}
